const {get} = require('lodash');
const FeeController = require('../controllers/fee.controller');

module.exports = async (job, done) => {
  const operation = get(job, 'data.operation');
  const payload = get(job, 'data.payload');

  try {
    switch(operation) {
      case 'UPDATE_AN_ACCOUNT_FEE':
        const response = await FeeController.handlers.event.updateAccountFee(payload);

        return done(null, response);
      default:
        done({ error: true, message: "Operation não mapeada" });
    }
  } catch (e) {
    console.log('---------- ERROR ----------');
    console.log(JSON.stringify(e));
    console.log('---------- ERROR ----------');

    done({
      message: e.message,
      error: e
    });
  }
};