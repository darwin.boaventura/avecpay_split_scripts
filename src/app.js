const express = require('express');
const bodyParser = require('body-parser');
const Arena = require('bull-arena');
const app = express();

const QueueService = require('./services/queue.service');

// Workers
QueueService.process(require('./workers/fee.worker'));

// Middleware
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use('/', Arena({
  queues: [
    {
      name: 'default',
      hostId: 'AvecPay',
      url: process.env.REDIS_URI
    },
  ],
},
{
  basePath: '/arena',
  disableListen: true
})
);

// Routers
app.use('/fee', require('./routers/fee.router'));

app.listen(process.env.SERVER_PORT, () => {
  console.log(`Server is running in port: ${process.env.SERVER_PORT}`);
});