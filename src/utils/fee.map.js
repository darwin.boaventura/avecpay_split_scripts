const FeeMAP = {
  "ELO": {
    'debit': 'Débito (ELO)',
    'credit': 'Crédito à vista (ELO)',
    'credit_2_to_6': 'Crédito 2 a 6 (ELO)',
    'credit_7_to_12': 'Crédito 7 a 12 (ELO)',
  },
  "AMEX": {
    'debit': 'Débito (AMEX)',
    'credit': 'Crédito à vista (AMEX)',
    'credit_2_to_6': 'Crédito 2 a 6 (AMEX)',
    'credit_7_to_12': 'Crédito 7 a 12 (AMEX)',
  },
  "Hipercard": {
    'debit': 'Débito (Hipercard)',
    'credit': 'Crédito à vista (Hipercard)',
    'credit_2_to_6': 'Crédito 2 a 6 (Hipercard)',
    'credit_7_to_12': 'Crédito 7 a 12 (Hipercard)',
  },
  "MasterCard": {
    'debit': 'Débito (MasterCard)',
    'credit': 'Crédito à vista (MasterCard)',
    'credit_2_to_6': 'Crédito 2 a 6 (MasterCard)',
    'credit_7_to_12': 'Crédito 7 a 12 (MasterCard)',
  },
  "Visa": {
    'debit': 'Débito (Visa)',
    'credit': 'Crédito à vista (Visa)',
    'credit_2_to_6': 'Crédito 2 a 6 (Visa)',
    'credit_7_to_12': 'Crédito 7 a 12 (Visa)',
  },
  "Hiper": {
    'debit': 'Débito (Hiper)',
    'credit': 'Crédito à vista (Hiper)',
    'credit_2_to_6': 'Crédito 2 a 6 (Hiper)',
    'credit_7_to_12': 'Crédito 7 a 12 (Hiper)',
  },
  "Maestro": {
    'debit': 'Débito (Maestro)',
    'credit': 'Crédito à vista (Maestro)',
    'credit_2_to_6': 'Crédito 2 a 6 (Maestro)',
    'credit_7_to_12': 'Crédito 7 a 12 (Maestro)',
  },
  "Banescard": {
    'debit': 'Débito (Banescard)',
    'credit': 'Crédito à vista (Banescard)',
    'credit_2_to_6': 'Crédito 2 a 6 (Banescard)',
    'credit_7_to_12': 'Crédito 7 a 12 (Banescard)',
  },
  "JCB": {
    'debit': 'Débito (JCB)',
    'credit': 'Crédito à vista (JCB)',
    'credit_2_to_6': 'Crédito 2 a 6 (JCB)',
    'credit_7_to_12': 'Crédito 7 a 12 (JCB)',
  },
  "Visa Electron": {
    'debit': 'Débito (Visa Electron)',
    'credit': 'Crédito à vista (Visa Electron)',
    'credit_2_to_6': 'Crédito 2 a 6 (Visa Electron)',
    'credit_7_to_12': 'Crédito 7 a 12 (Visa Electron)',
  },
  "Diners Club": {
    'debit': 'Débito (Diners Club)',
    'credit': 'Crédito à vista (Diners Club)',
    'credit_2_to_6': 'Crédito 2 a 6 (Diners Club)',
    'credit_7_to_12': 'Crédito 7 a 12 (Diners Club)',
  }
};

module.exports = FeeMAP;