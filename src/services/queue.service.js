const bull = require('bull');

const queue = new bull('default', process.env.REDIS_URI);

queue.on('error', (error) => {
  console.log('============= QUEUE ERROR =============');
  console.log(error);
  console.log('============= QUEUE ERROR =============');
})

module.exports = queue;