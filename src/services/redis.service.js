const bluebird = require('bluebird');
const redis = require('redis');

bluebird.promisifyAll(redis);

const client = redis.createClient({
  url: process.env.REDIS_URI
});

client.on('error', (err) => {
  console.log('---------- ERROR ON REDIS ----------');
  console.log(err);
  console.log('---------- ERROR ON REDIS ----------');
});

client.on('ready', () => {
  console.log('Redis is ready');
});

module.exports = client;