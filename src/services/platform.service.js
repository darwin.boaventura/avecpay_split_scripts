const _ = require('lodash');
const axios = require('axios');

const PlatformService = {
  baseURL: process.env.AVECPAY_API_URL,
  baseConfig: {
    headers: {
      Authorization: `Bearer ${process.env.AVECPAY_API_TOKEN}`
    }
  },

  updateFee: async (document, fee) => {
    const requisition = await axios.patch(`${PlatformService.baseURL}/fee/account/${document}`, fee, PlatformService.baseConfig);

      return requisition.data;
  },

  getAccountByDocument: async (document) => {
    try {
      const requisition = await axios.get(`${PlatformService.baseURL}/account/${document}`, PlatformService.baseConfig);

      return requisition.data;
    } catch (e) {
      if (_.get(e, 'response.status') === 404) {
        return undefined;
      }

      throw e;
    }
  }
};

module.exports = PlatformService;