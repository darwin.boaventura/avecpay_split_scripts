const express = require('express');
const router = express.Router();

const FeeController = require('../controllers/fee.controller');

router.put('/', FeeController.handlers.http.updateAccountFee);
router.get('/failed', FeeController.handlers.http.getFailedOperations);
router.post('/transform', FeeController.handlers.http.transformOriginalPayload);
 
module.exports = router;