const _ = require('lodash');
const bluebird = require('bluebird');

const QueueService = require('../services/queue.service');
const RedisService = require('../services/redis.service');
const PlatformService = require('../services/platform.service');

const FlagCardENUM = require('../enums/flagcard.enum');
const FeeMap = require('../utils/fee.map');

const FeeController = {
  setFlagCardId: (type) => {
    if (String(type).toLowerCase().includes('elo')) {
      return 'elo';
    }
    
    if (String(type).toLowerCase().includes('mastercard')) {
      return 'mastercard';
    }

    if (String(type).toLowerCase().includes('visa)')) {
      return 'visa';
    }
    
    if (String(type).toLowerCase().includes('amex')) {
      return 'amex';
    }
    
    if (String(type).toLowerCase().includes('hipercard')) {
      return 'hipercard';
    }
    
    if (String(type).toLowerCase().includes('visa electron')) {
      return 'visa_electron';
    }
    
    if (String(type).toLowerCase().includes('hiper)')) {
      return 'hiper';
    }
    
    if (String(type).toLowerCase().includes('maestro')) {
      return 'maestro';
    }
    
    if (String(type).toLowerCase().includes('banescard')) {
      return 'banescard';
    }
    
    if (String(type).toLowerCase().includes('jcb')) {
      return 'jcb';
    }
    
    if (String(type).toLowerCase().includes('diners club')) {
      return 'diners_club';
    }

    return 'default';
  },

  setPaymentType: (type) => {
    const lowerType = String(type).toLowerCase();

    if (lowerType.includes('débito')) {
      return 1;
    } else if (lowerType.includes('crédito')) {
      return 2;
    } else if (lowerType.includes('antecipação')) {
      return 5;
    }
  },

  groupFeeByDocument: (data) => {
    return _.groupBy(data, (item) => item.documento);
  },

  setAccountMetadata: (data) => {
    const accounts = {};

    for (let document in data) {
      const fees = data[document];

      accounts[document] = {};
      accounts[document]['metadata'] = {};
      accounts[document]['data'] = fees;

      for (let flag in FeeMap) {
        const flagOptions = FeeMap[flag];

        accounts[document]['metadata'][flag] = {};

        for (let option in flagOptions) {
          accounts[document]['metadata'][flag][option] = Boolean(_.find(fees, (fee) => String(fee.tipo_taxa).includes(flagOptions[option])));
        }
      }
    }

    return accounts;
  },

  setDefaultFeeByAccount: (data) => {
    const feeByAccount = data;

    for (let document in feeByAccount) {
      let handled = [];

      for (let item of feeByAccount[document]) {
        const fee = {
          ...item,
          quantidade_parcelas: 1
        };

        if (!item.tipo_taxa.includes('Crédito 2 a 6') && !item.tipo_taxa.includes('Crédito 7 a 12')) {
          handled.push(fee);
        }

        if (String(item.tipo_taxa).includes('2 a 6')) {
          for (let i = 2; i <= 6; i++) {
            handled.push(Object.assign({}, fee, { quantidade_parcelas: i }));
          }
        }
        
        if (String(item.tipo_taxa).includes('7 a 12')) {
          for (let i = 7; i <= 12; i++) {
            handled.push(Object.assign({}, fee, { quantidade_parcelas: i }));
          }
        }
      }

      const defaultType = ['Débito', 'Crédito à vista', 'Crédito 2 a 6', 'Crédito 7 a 12', 'Antecipação'];

      handled = handled.map((item) => {
        item.default = defaultType.includes(item.tipo_taxa);

        return item;
      });

      feeByAccount[document] = handled;
    }

    return feeByAccount;
  },

  includeMissingCards: (data) => {
    const feeByAccount = data;

    for (let document in feeByAccount) {
      const accountFee = feeByAccount[document]['data'];
      const accountDefaultFee = _.uniqBy(_.filter(feeByAccount[document]['data'], (fee) => fee.default), (fee) => fee.id);

      for (let card in feeByAccount[document]['metadata']) {
        if (!feeByAccount[document]['metadata'][card]['debit']) {
          const defaultDebitFee = _.find(accountDefaultFee, (fee) => fee.tipo_taxa === 'Débito');
          
          accountFee.push(Object.assign({}, defaultDebitFee, {
            tipo_taxa: `${defaultDebitFee.tipo_taxa} (${card})`
          }));
        }

        if (!feeByAccount[document]['metadata'][card]['credit']) {
          const defaultCreditFee = _.find(accountDefaultFee, (fee) => fee.tipo_taxa === 'Crédito à vista');

          accountFee.push(Object.assign({}, defaultCreditFee, {
            tipo_taxa: `${defaultCreditFee.tipo_taxa} (${card})`
          }));
        }
        
        if (!feeByAccount[document]['metadata'][card]['credit_2_to_6']) {
          const defaultCredit2to6Fee = _.find(accountDefaultFee, (fee) => fee.tipo_taxa === 'Crédito 2 a 6');

          accountFee.push(Object.assign({}, defaultCredit2to6Fee, {
            tipo_taxa: `${defaultCredit2to6Fee.tipo_taxa} (${card})`
          }));
        }
        
        if (!feeByAccount[document]['metadata'][card]['credit_7_to_12']) {
          const defaultCredit7to12Fee = _.find(accountDefaultFee, (fee) => fee.tipo_taxa === 'Crédito 7 a 12');

          accountFee.push(Object.assign({}, defaultCredit7to12Fee, {
            tipo_taxa: `${defaultCredit7to12Fee.tipo_taxa} (${card})`
          }));
        }
      }

      feeByAccount[document] = accountFee;
    }

    return feeByAccount;
  },

  removeNotUsedFeeField: (data) => {
    const handled = {};

    for (let document in data) {
      handled[document] = [];

      for (let fee of data[document]) {
        handled[document].push({
          documento: fee.documento,
          tipo_taxa: fee.tipo_taxa,
          quantidade_parcelas: fee.quantidade_parcelas,
          valor: fee.valor,
        });
      }
    }

    return handled;
  },

  flattenFee: (data) => {
    const fee = [];
    
    for (let document in data) {
      for (let item of data[document]) {
        fee.push(item);
      }
    }

    return fee;
  },

  setInstallmentsFee: (data) => {
    const accounts = {};

    for (let document in data) {
      accounts[document] = [];

      for (let fee of data[document]) {
        if (fee.tipo_taxa.includes('2 a 6')) {
          for (let i = 2; i <= 6; i++) {
            accounts[document].push(Object.assign({}, fee, {
              quantidade_parcelas: i
            }));
          }
        } else if (fee.tipo_taxa.includes('7 a 12')) {
          for (let i = 7; i <= 12; i++) {
            accounts[document].push(Object.assign({}, fee, {
              quantidade_parcelas: i
            }));
          }
        } else {
          accounts[document].push(fee);
        }
      }
    }

    return accounts;
  },

  handlers: {
    http: {
      updateAccountFee: async (req, res) => {
        try {
          const fees = req.body;

          if (fees) {
            for (let document in fees) {
              QueueService.add({
                operation: 'UPDATE_AN_ACCOUNT_FEE',
                payload: {
                  document: document,
                  fees: fees[document]
                }
              });
            }
          }
    
          return res.json({ message: 'Fee was added to queue and will be updated as soon as possible!' });
        } catch (e) {
          return res.end(e);
        }
      },
  
      transformOriginalPayload: async (req, res) => {
        const payload = req.body;
  
        if (Array.isArray(payload)) {
          let feeByAccount;
          
          feeByAccount = FeeController.groupFeeByDocument(payload);
          feeByAccount = FeeController.setDefaultFeeByAccount(feeByAccount);
          feeByAccount = FeeController.setAccountMetadata(feeByAccount);
          feeByAccount = FeeController.includeMissingCards(feeByAccount);
          feeByAccount = FeeController.removeNotUsedFeeField(feeByAccount);
          feeByAccount = FeeController.setInstallmentsFee(feeByAccount);
  
          return res.send(feeByAccount);
        }
  
        return res.status(400).send({ message: `Should recieve a fee's array` });
      },

      getFailedOperations: async (req, res) => {
        const errors = [];

        const keys = await RedisService.keysAsync('*');

        if (keys && Array.isArray(keys)) {
          const operations = [];

          for (let item of keys) {
            try {
              if (item !== 'bull:default:failed') {
                const operation = await RedisService.hgetallAsync(`${item}`);

                operations.push(operation);
              }
            } catch (err) {
              errors.push(err);
            }
          }

          delete errors;

          const failedOperations = _.filter(operations, (operation) => operation.failedReason);
          const groupedOperationByDocument = _.groupBy(failedOperations, (operation) => {
            const data = JSON.parse(operation.data);

            return data.payload.documento;
          });

          const formattedOperations = {};

          for (let document in groupedOperationByDocument) {
            formattedOperations[document] = _.map(groupedOperationByDocument[document], (operation) => (JSON.parse(operation.data)).payload);
          }          

          return res.json(formattedOperations);
        }

        return res.status(500).send({ message: 'Something went wrong!' });
      }
    },

    event: {
      updateAccountFee: async (data) => {
        const fees = !Array.isArray(data.fees) ? [] : data.fees.map((fee) => {
          return {
            flagCardId: FlagCardENUM[FeeController.setFlagCardId(fee.tipo_taxa)],
            paymentType: FeeController.setPaymentType(fee.tipo_taxa),
            installmentNumber: fee.quantidade_parcelas,
            feeValue: fee.valor,
            active: 1
          };
        });

        const response = await PlatformService.updateFee(data.document, fees);
    
        return {
          account: {
            document: data.document
          },
          fees: response
        };
      }, 
    }
  }
};

module.exports = FeeController;