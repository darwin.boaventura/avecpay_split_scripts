const FlagCardENUM = {
  default: 1,
  mastercard: 2,
  visa: 3,
  elo: 4,
  amex: 5,
  hipercard: 6,
  visa_electron: 7,
  hiper: 8,
  maestro: 9,
  banescard: 10,
  jcb: 11,
  diners_club: 12,
};

module.exports = FlagCardENUM;