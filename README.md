## Scripts de suporte para AvecPay (Plataforma)

###  Como rodar essa aplicação?

- Clone o projeto
- Rode `npm install` na raiz do projeto
- Rode `docker-compose up`

### Como atualizar todas as taxas?

- Exporte todas as taxas no banco do Salão Vip como JSON
- Envie o JSON para o endpoint `POST http://localhost:4000/fee/transform`
- Envie a resposta da última requisição para o endpoint `PUT http://localhost:4000/fee`
- Acompanhe a fila em `http://localhost:4000/arena`